from socketserver import BaseRequestHandler, TCPServer;
import threading

SERVER_HOST = "localhost"
SERVER_PORT = 1234
SERVER_ADDRESS = (SERVER_HOST, SERVER_PORT)

class ElChatRequestHandler(BaseRequestHandler):
    def handle(self):
        client_ip = self.client_address[0]
        client_sent_data = data = self.request.recv(1024)
        response = "%s said: %s" % (client_ip, client_sent_data)
        self.request.send(response.encode())


class ElChatServer(TCPServer):
    def __init__(self):
        TCPServer.__init__(self, SERVER_ADDRESS, ElChatRequestHandler)
        return


if __name__ == "__main__":
    app = ElChatServer()
    app.serve_forever()
    t = threading.Thread(target=app.serve_forever)
    t.setDaemon(True)
    t.start()