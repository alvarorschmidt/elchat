import tkinter
import threading

from WebClient import WebClient

QUIT_MSG = "{quit}"

class ElChat():
    def __init__(self, master):
        self.master = master;
        self.master.title = "Bem-Vindo ao El Chat"

        self.web_client = WebClient(self.receive_msg)

        self.msg_frame = tkinter.Frame(self.master)
        self.msg_to_be_sent = tkinter.StringVar()
        self.msg_to_be_sent.set("Escreva suas mensagens aqui: ")
        self.scrollbar = tkinter.Scrollbar(self.master)

        self.msg_list = tkinter.Listbox(self.msg_frame, height=15, width=50, yscrollcommand=self.scrollbar.set)
        self.scrollbar.pack(side=tkinter.RIGHT, fill=tkinter.Y)
        self.msg_list.pack(side=tkinter.LEFT, fill=tkinter.BOTH)
        self.msg_list.pack()
        self.msg_frame.pack()

        self.entry_field = tkinter.Entry(self.master, textvariable=self.msg_to_be_sent)
        self.entry_field.bind("<Return>", self.send_msg)
        self.entry_field.pack()

        self.send_button = tkinter.Button(self.master, text="Send", command=self.send_msg)
        self.send_button.pack()

        self.master.protocol("WM_DELETE_WINDOW", self.on_closing)

    def send_msg(self):
        print("sending something")
        msg = self.msg_to_be_sent.get()
        self.web_client.send(msg)
        if msg == QUIT_MSG:
            self.on_closing()
            self.master.quit()

    def receive_msg(self, msg):
        self.msg_list.insert(tkinter.END, msg)

    def on_closing(self):
        self.msg_to_be_sent.set(QUIT_MSG)


if __name__ == "__main__":
    root = tkinter.Tk()
    ElChat(root)
    root.mainloop()

