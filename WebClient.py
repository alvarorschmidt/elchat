import socket
import threading

def get_connection():
    print("getting a new connection.")
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect(("localhost", 1234))
    return sock

class WebClient:
    def __init__(self, msg_channel):
        print("initializing a new webclient.")
        self.msg_channel = msg_channel;

    def send(self, data):
        print("web client sending data: " + str(data))
        connection = get_connection()
        connection.sendall(bytes(data, "utf-8"))
        print("data's been sent by web client.")
        while True:
            try:
                msg = connection.recv(1024).decode("utf-8")
                print("web client received: " + str(msg))
                if not msg:
                    print("web client loose its connection with server.")
                    break
                else:
                    print("web client sending received data through msg_channel.")
                    self.msg_channel(msg)
                    break
            except OSError:
                break
        return msg


if __name__ == "__main__":
    app = WebClient()